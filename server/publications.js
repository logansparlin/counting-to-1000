import {People} from '../lib/collections'

Meteor.publish('people', () => {
  return People.find()
})
