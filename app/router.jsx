import React from 'react'
import {mount} from 'react-mounter'

import AppLayout from './AppLayout'
import Crew from './containers/Crew'
import Home from './components/Home'
import Screenings from './components/screenings/Screenings'

FlowRouter.route('/', {
  name: 'home',
  action() {
    mount(AppLayout, {
      content: <Home />
    })
  }
})

FlowRouter.route('/crew', {
  name: 'crew',
  action() {
    mount(AppLayout, {
      content: <Crew />
    })
  }
})

FlowRouter.route('/screenings', {
  name: 'screenings',
  action() {
    mount(AppLayout, {
      content: <Screenings />
    })
  }
})


FlowRouter.route('/:day', {
  name: 'day',
  action(params) {
    mount(AppLayout, {
      content: <Home activeDay={params.day} fromRoute={true}/>
    })
  }
})
