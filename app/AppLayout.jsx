import React, {Component} from 'react'
import Nav from './components/nav/Nav'
import NavIcon from './components/nav/NavIcon'
import classNames from 'classnames'

export default class Home extends Component {

	constructor(props) {
		super(props)
		this.state = {
			nav: false
		}
		this.toggleNav = this.toggleNav.bind(this)
	}

	toggleNav() {
		this.setState({ nav: !this.state.nav })
	}

  render() {
    let {content} = this.props;

    let classes = classNames({'nav--open': (this.state.nav)})

    return (
      <div className={classes}>
      	<NavIcon toggleNav={this.toggleNav} />
        <Nav toggleNav={this.toggleNav} />
        <div className="content-container">
        	{content}
        </div>
      </div>
    )
  }
}
