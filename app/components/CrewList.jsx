import React, {Component} from 'react'

export default class CrewList extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let {people} = this.props

    // // loop over this array and preload all images
    // let images = people.map(person => {
    //   return person.image
    // })

    return (
      <div className="people-list">
        {people.map((person) => {
          return (
            <div key={person._id} className="person">
              <img src={person.image.url} />
              <span className="person-info">
                <span className="name">{person.name}</span>
                <span className="job-title">{person.job_title}</span>
              </span>
            </div>
          )
        })}
      </div>
    )
  }
}
