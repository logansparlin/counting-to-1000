import React, {Component} from 'react'

export default class Loading extends Component {

  componentDidMount() {
    let el = this.refs["cto-title"]
    TweenMax.to(el, 2, {
      scale: 0.3,
      opacity: 1,
      ease: Power0.easeIn,
      delay: 0.2
    })
  }

  render() {
    return (
      <div className="loading-container">
        <image ref="cto-title" className="cto-title" src="/cto-title.svg" />
      </div>
    )
  }
  
}
