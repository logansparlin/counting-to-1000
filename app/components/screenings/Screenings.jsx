import React, {Component} from 'react'

export default class Screenings extends Component {

	render() {
		return (
			<div className="screenings-container">
				<div className="overlay" />
				<div className="screenings-content">
					<h1>Screenings</h1>
					<div className="screening-item">
						<div className="inner">
							<span className="date">May 08</span>
							<h3 className="location">The Moxie</h3>
						</div>
					</div>
					<div className="screening-item">
						<div className="inner">
							<span className="date">May 09</span>
							<h3 className="location">MSU EA Showcase</h3>
						</div>
					</div>
				</div>
			</div>
		)
	}

}