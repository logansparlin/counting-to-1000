import React, {Component} from 'react'
import Pager from './pager/Pager'
import classNames from 'classnames'
import Content from './content/Content'
import _ from 'underscore'
import days from '../days'

export default class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      index: this.getIndexBy(days, "slug", props.activeDay) || 0,
      currentIndex: this.getIndexBy(days, "slug", props.activeDay) || 0,
      animating: false,
      move: (!props.activeDay) ? 'in' : null,
      activeDay: props.activeDay || null
    }

    this.changeIndex = this.changeIndex.bind(this)
    this.onWheel = _.debounce(this.onWheel, 75, true).bind(this)
    this.renderBackgroundImage = this.renderBackgroundImage.bind(this)
  }

  getIndexBy(list, key, value) {
    for (var i = 0; i < list.length; i++) {
        if (list[i][key] == value) {
            return i;
        }
    }
    return false;
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.activeDay && nextProps.activeDay !== this.props.activeDay) {
      this.setState({
        activeDay: nextProps.activeDay,
        index: this.getIndexBy(days, "slug", nextProps.activeDay),
        move: 'out'
      })
    }
    else {
      this.setState({
        activeDay: null,
        move: 'in'
      })
    }
  }

  pickDay(slug) {
    FlowRouter.go('/' + slug)
  }

  changeIndex(index) {
    this.setState({ 
      move: 'out', 
      index: index,
      animating: true
    })
    setTimeout(() => {
      this.setState({
        currentIndex: index, 
        move: "in",
        animating: false
      })
    }, 600)
  }

  onWheel(e) {
    if(!this.state.activeDay) {
      let delta = e.deltaY;
      let {index} = this.state;

      if(delta >= 1) {
        // Next
        let newIndex = index += 1;
        if(newIndex < days.length) {
          this.changeIndex(newIndex)
        }
      } else if(delta <= -1) {
        // Previous
        let newIndex = index -= 1;
        if(newIndex >= 0) {
          this.changeIndex(newIndex)
        }
      }
    }
  }

  renderBackgroundImage(index) {
    let {animating, currentIndex} = this.state;
    if(animating) {
      return (
          <div key="bg-image-container" className="bg-image-container">
            <div className="bg-image" style={{ background: 'url(/img/' + days[currentIndex].image + ') no-repeat center center', 'background-size': 'cover'}}></div>
            <div className="bg-image primary fade-in" style={{ background: 'url(/img/' + days[index].image + ') no-repeat center center', 'background-size': 'cover'}}></div>
          </div>
        )
    } 
    else {
      return (
        <div key="bg-image-container" className="bg-image-container">
          <div className="bg-image" style={{ background: 'url(/img/' + days[currentIndex].image + ') no-repeat center center', backgroundSize: 'cover'}}></div>
          <div className="bg-image" style={{ background: 'url(/img/' + days[index].image + ') no-repeat center center', backgroundSize: 'cover'}}></div>
        </div>
      )
    }
  }

  render() {
    let {index, move, currentIndex} = this.state;
    let day = days[currentIndex];
    let animateClasses = classNames({'move-in': this.state.move == "in"}, {'move-out': this.state.move == "out"})
    let homeClasses = classNames("cto-home", {'day-active': (this.state.activeDay)})

    return (
      <div className={homeClasses} onWheel={this.onWheel}>
        {this.renderBackgroundImage(index)}
        <div className="overlay"></div>
        <span className="cto-logo">Counting To 1000</span>
        <div className='cto-home-content'>
          <div className="overflow-wrap">
            <h3 className={`day ${animateClasses}`}>{`Day ${day.number}`}</h3>
          </div>
          <div className="overflow-wrap title-wrap">
            <h1 className={`title ${animateClasses}`}>{day.title}</h1>
          </div>
          <div className="overflow-wrap">
            <button onClick={this.pickDay.bind(this, day.slug)} className={`explore-button ${animateClasses} ${(this.state.activeDay == day.slug) ? 'active' : null}`}>
              <span className="button-inner" />
              <span className="button-text">{`Explore Day ${day.number}`}</span>
            </button>
          </div>
        </div>
        <Pager days={days} index={index} changeIndex={this.changeIndex}/>
        <Content day={days[index]}/>
      </div>
    )
  }
}
