import React, {Component} from 'react'

export default class Footer extends Component {
	render() {
		return (
			<footer>
				<span className="copyright">Copyright © 2016 Counting to 1000. All Rights Reserved.</span>
			</footer>
		)
	}
}