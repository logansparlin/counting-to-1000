import React, {Component} from 'react'
import Footer from '../footer/Footer'

export default class Content extends Component {
	render() {
		let {day} = this.props
		return (
			<div className="main-content">
				<div className="intro">
					<h1 className="day">{`Production Day ${day.number}`}</h1>
					<h2 className="title">{day.title}</h2>
					<div className="info-container container">
						<div className="third">
							<span className="info-label">Date</span>
							<span className="info-text">{day.date}</span>
						</div>
						<div className="third">
							<span className="info-label">Location</span>
							<span className="info-text">South Roanoke Drive</span>
						</div>
						<div className="third">
							<span className="info-label">Scenes</span>
							<span className="info-text">01, 03, 05, 08</span>
						</div>
					</div>
				</div>
				<div className="clearfix"></div>
				<div className="images-container">
					<div className="image-section left">
						<div className="image-container">
							<img className="image" src="/img/day6/day6-1.jpg" />
						</div>
						<div className="image-caption">Ivy V. Yarckow-Brown and students from the Missouri State University Criminology and Criminal Justice Department helped us create realistic crime scenes on several sets.</div>
					</div>
					<div className="image-section right">
						<div className="image-caption">Special effects involving the rain machine calls for protecting our equipment.</div>
						<div className="image-container">
							<img className="image" src="/img/day6/day6-2.jpg" />
						</div>
					</div>
					<div className="image-section left">
						<div className="image-container">
							<img className="image" src="/img/day6/day6-3.jpg" />
						</div>
						<div className="image-caption">Special effects involving the rain machine calls for protecting our equipment.</div>
					</div>
					<div className="image-section right">
						<div className="image-caption">“Prep the truck, get the crew. We’re taking it tonight.”</div>
						<div className="image-container">
							<img className="image" src="/img/day6/day6-4.jpg" />
						</div>
					</div>
					<div className="image-section left">
						<div className="image-container">
							<img className="image" src="/img/day6/day6-5.jpg" />
						</div>
						<div className="image-caption">Camera crew watch the scene unfold on day six.</div>
					</div>
				</div>

				<Footer />
			</div>
		)
	}
}