import React, {Component} from 'react'

export default class NavIcon extends Component {

	render() {
		let {toggleNav} = this.props
		return (
			<div className="nav-icon" onClick={toggleNav}>
				<div className="inner">
					<span className="bar" />
					<span className="bar" />
					<span className="bar" />
				</div>
			</div>
		)
	}

}