import React, {Component} from 'react'

export default class Nav extends Component {
  render() {
    let {toggleNav} = this.props;
    return (
      <div>
        <div className="nav-overlay" />
        <div id="main-nav">
          <nav>
            <img className="nav-logo" src="/cto-title-dark.svg" />
            <ul>
              <li>
              	<a onClick={toggleNav} href="/">Home</a>
              </li>
              <li>
              	<a onClick={toggleNav} href="/crew">Cast & Crew</a>
              </li>
              <li>
                <a onClick={toggleNav} href="/screenings">
                  Screenings
                </a>
              </li>
              <li>Trailers</li>
              <li>Credits</li>
            </ul>
          </nav>

          <div className="social">
            <i className="fa fa-facebook" aria-hidden="true"></i>
            <i className="fa fa-instagram" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    )
  }
}
