import React, {Component} from 'react'
import classNames from 'classnames'

export default class Pager extends Component {

	render() {
		let {index, changeIndex, days} = this.props

		function classes(pagerIndex) {
			return classNames("pager-item", {active: pagerIndex == index})
		}

		return (
			<div className="pager">
				{days.map((day, index) => {
					return (
						<div key={day.number} className={classes(index)} onClick={changeIndex.bind(null, index)}>
							<span className="pager-bar" />
							<span className="day-label">{`${day.number}`}</span>
						</div>
					)
				})}
			</div>
		)
	}

}