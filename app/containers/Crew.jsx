import {composeWithTracker} from 'react-komposer'
import CrewList from '../components/CrewList'
import {People} from '../../lib/collections'

function composer(props, onData) {
  const handle = Meteor.subscribe('people');
  if(handle.ready) {
    const people = People.find({}, {sort: {category: -1} }).fetch()
    onData(null, {people})
  }
}

export default composeWithTracker(composer)(CrewList)
