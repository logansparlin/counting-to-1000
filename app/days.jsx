const days = [
  {
    number: "01",
    title: "The Beginning",
    slug: "the-beginning",
    image: "day-1.jpg",
    date: "Saturday, October 10th"
  },
  {
    number: "02",
    title: "The Job",
    slug: "the-job",
    image: "day-2.jpg",
    date: "Sunday, October 11th"
  },
  {
    number: "03",
    title: "The Entry",
    slug: "the-entry",
    image: "day-3.jpg",
    date: "Saturday, November 7th"
  },
  {
    number: "04",
    title: "The Struggle",
    slug: "the-struggle",
    image: "day-4.jpg",
    date: "Sunday, November 8th"
  },
  {
    number: "05",
    title: "The Search",
    slug: "the-search",
    image: "day-5.jpg",
    date: "Friday, November 13th"
  },
  {
    number: "06",
    title: "The Morning",
    slug: "the-morning",
    image: "day-6.jpg",
    date: "Saturday, November 14th"
  },
  {
    number: "07",
    title: "The Investigation",
    slug: "the-investigation",
    image: "day-7.jpg",
    date: "Saturday, November 21st"
  },
  {
    number: "08",
    title: "The Interrogation",
    slug: "the-interrogation",
    image: "day-8.jpg",
    date: "Sunday, February 21st"
  }
]

export default days